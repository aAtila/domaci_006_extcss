#Domaći zadatak

Domaći zadatak 'domaci_005_pagewithform' proširiti na sledeći način:

* koristiti 3 vrste fonta (1 font da bude Arial a druga dva po izboru sa Google Fonts sajta).
* Tekst treba da bude ispisan u nekoj proizvoljnoj boji (obavezno preko hexadecimalnih vrednosti).
* Za linkove podesiti sva 4 stanja.
* Linkovi ka omiljenim stranicama treba da budu drugačijeg stila od primarnih linkova (obavezno koristiti klasu). 
* Za slike koristiti float atribut.
* Obavezno koristiti pozadinsku boju i pozadinsku sliku.
* Obavezno korišćenje spoljašnjeg CSS fajla.